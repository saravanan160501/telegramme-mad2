//
//  ProfileViewController.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 8/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileName.text =  (UserDefaults.standard.string(forKey: "loggedInFirstName") ?? "") + " " + (UserDefaults.standard.string(forKey: "loggedInLastName") ?? "")
        mobileNumber.text = UserDefaults.standard.string(forKey: "loggedInMobileNumber")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

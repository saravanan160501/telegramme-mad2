//
//  ViewController.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 7/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    var contacts: [NSManagedObject] = []
    
    @IBOutlet weak var usrNameFld: UITextField!
    @IBOutlet weak var pwdFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usrNameFld.text = UserDefaults.standard.string(forKey: "loggedInFirstName")
        pwdFld.text = UserDefaults.standard.string(forKey: "loggedInMobileNumber")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchContacts()
    }
    
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func fetchContacts() {
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Contacts")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        
        do {
            contacts = try context.fetch(fetchRequest)
        } catch let error as NSError {
            let errorDialog = UIAlertController(title: "Error!", message: "Failed to save! \(error): \(error.userInfo)", preferredStyle: .alert)
            errorDialog.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            present(errorDialog, animated: true)
        }
    }
    
    @IBAction func loginBtn(_ sender: UIButton) {
        contacts.forEach {
            if ($0).value(forKey: "firstName") as? String == usrNameFld.text && ($0).value(forKey: "mobileNumber") as? String == pwdFld.text{
                UserDefaults.standard.set(($0).value(forKey: "firstName") as? String, forKey: "loggedInFirstName")
                UserDefaults.standard.set(($0).value(forKey: "lastName") as? String, forKey: "loggedInLastName")
                UserDefaults.standard.set(($0).value(forKey: "mobileNumber") as? String, forKey: "loggedInMobileNumber")
                performSegue(withIdentifier: "Content", sender: self)
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Invalid Credentials", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                
                self.present(alert, animated: true)
            }
        }
    }
}


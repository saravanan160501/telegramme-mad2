//
//  ContactsTableViewController.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 8/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController: UITableViewController {
    
    var contacts: [NSManagedObject] = []
    var selectedContactObject = NSManagedObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchContacts()
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell", for: indexPath)
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = (contact.value(forKeyPath: "firstName") as? String)! + " " + (contact.value(forKeyPath: "lastName") as? String)!
        cell.detailTextLabel?.text = (contact.value(forKeyPath: "mobileNumber") as? String)!
        return cell
    }
    
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func fetchContacts() {
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Contacts")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true)]
        
        do {
            contacts = try context.fetch(fetchRequest)
        } catch let error as NSError {
            let errorDialog = UIAlertController(title: "Error!", message: "Failed to save! \(error): \(error.userInfo)", preferredStyle: .alert)
            errorDialog.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            present(errorDialog, animated: true)
        }
    }
    
    func removeContact(fetchRequest1: NSManagedObject) {
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Contacts")
        fetchRequest.predicate = NSPredicate(format: "mobileNumber = %@", fetchRequest1.value(forKey: "mobileNumber") as! String)
        
        do {
            let objects = try context.fetch(fetchRequest)
            for object in objects {
                context.delete(object)
            }
            try context.save()
        } catch _ {
            // error handling
        }
        
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            removeContact(fetchRequest1: contacts[indexPath.row])
            self.contacts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    @IBAction func addContactBtn(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "AddContact", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Segue to the second view controller
        selectedContactObject = contacts[indexPath.row]
        self.performSegue(withIdentifier: "UpdateContact", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UpdateContact"{
            // get a reference to the second view controller
            let UpdateContactViewController = segue.destination as! UpdateContactViewController
            
            // set a variable in the second view controller with the String to pass
            UpdateContactViewController.contactObject = selectedContactObject
        }
    }
}

//
//  UpdateContactViewController.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 8/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

import UIKit
import CoreData

class UpdateContactViewController: UIViewController {
    
    var contactObject = NSManagedObject()
    
    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    @IBOutlet weak var mobileNumberFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameFld.text = contactObject.value(forKey: "firstName") as? String
        lastNameFld.text = contactObject.value(forKey: "lastName") as? String
        mobileNumberFld.text = contactObject.value(forKey: "mobileNumber") as? String
        
        // Do any additional setup after loading the view.
    }
    
    func UpdateContact(identifier: String, firstName:String, lastName:String, mobileNumber:String) {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Contacts")
        let predicate = NSPredicate(format: "mobileNumber = '\(identifier)'")
        fetchRequest.predicate = predicate
        do
        {
            let object = try context.fetch(fetchRequest)
            if object.count == 1
            {
                let objectUpdate = object.first as! NSManagedObject
                objectUpdate.setValue(firstName, forKey: "firstName")
                objectUpdate.setValue(lastName, forKey: "lastName")
                objectUpdate.setValue(mobileNumber, forKey: "mobileNumber")
                do{
                    try context.save()
                }
                catch
                {
                    print(error)
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    
    @IBAction func updateContactBtn(_ sender: UIButton) {
        if mobileNumberFld.text != ""{
            UpdateContact(identifier: contactObject.value(forKey: "mobileNumber") as? String ?? "", firstName: firstNameFld.text ?? "", lastName: lastNameFld.text ?? "", mobileNumber: mobileNumberFld.text!)
            
            let alert = UIAlertController(title: "Update Contact", message: "Contact has been successfully updated", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Error", message: "Key in the Mobile Number", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
}

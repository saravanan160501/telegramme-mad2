//
//  AddContactViewController.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 8/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

import UIKit
import CoreData

class AddContactViewController: UIViewController {

    @IBOutlet weak var firstNameFld: UITextField!
    @IBOutlet weak var lastNameFld: UITextField!
    @IBOutlet weak var mobileNumberFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    func addContact(_ contactObject: Contact) {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Contacts", in: context)
        let contact = NSManagedObject(entity: entity!, insertInto: context)
        
        contact.setValue(contactObject.firstName, forKey: "firstName")
        contact.setValue(contactObject.lastName, forKey: "lastName")
        contact.setValue(contactObject.mobileNumber, forKey: "mobileNumber")
    }
    
    @IBAction func addContactBtn(_ sender: UIButton) {
        if mobileNumberFld.text != ""{
            addContact(Contact(firstName: firstNameFld.text ?? "", lastName: lastNameFld.text ?? "", mobileNumber: mobileNumberFld.text!))
            
            let alert = UIAlertController(title: "Add Contact", message: "Contact has been successfully added", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Proceed", style: .default, handler: nil))

            self.present(alert, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Error", message: "Key in the Mobile Number", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

            self.present(alert, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  Contact.swift
//  Telegramme
//
//  Created by Mohan Saravanan on 8/12/19.
//  Copyright © 2019 Mohan Saravanan. All rights reserved.
//

class Contact {
    var firstName: String
    var lastName: String
    var mobileNumber: String
    
    init(firstName: String, lastName: String, mobileNumber: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.mobileNumber = mobileNumber
    }
}
